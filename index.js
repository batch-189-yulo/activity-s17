/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	

	//first function here: 
	function userName(){
		let name = prompt('Enter your name.');
		console.log('Hello!, ' + name + '!');
	}
	userName();

	function userAge(){
		let age = prompt('Please enter your age.');
		console.log('You are ' + age + ' years young.');
	}
	userAge();
	function userLocation(){
		let location = prompt('Please enter the city that you live in.');
		console.log('You live in ' + location + '.');
	}
	userLocation();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favBand1(){
	let band1 = '1. Parokya ni Edgar'
	console.log(band1)
}
favBand1()

	function favBand2(){
	let band2 = '2. Kamikazee'
	console.log(band2)
}
favBand2()

	function favBand3(){
	let band3 = '3. Eraserheads'
	console.log(band3)
}
favBand3()

	function favBand4(){
	let band4 = '4. Mayonnaise'
	console.log(band4)
}
favBand4()

	function favBand5(){
	let band5 = '5. Silk Sonic'
	console.log(band5)
}
favBand5()






/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	let favMovie1 = function(){
	console.log('1. Avengers: End Game')
}
	favMovie1()

	let rottenTomato1 = function(){
	console.log('Rotten Tomatoes Rating: 94%')
}
	rottenTomato1()

	let favMovie2 = function(){
		console.log('2. Avengers: Infinity Wars')
}
	favMovie2()

	let rottenTomato2 = function(){
	console.log('Rotten Tomatoes Rating: 85%')
}
	rottenTomato2()

	let favMovie3 = function(){
		console.log('3. Guardians Of The Galaxy')
}
	favMovie3()

	let rottenTomato3 = function(){
	console.log('Rotten Tomatoes Rating: 92%')
}
	rottenTomato3()

	let favMovie4 = function(){
		console.log('4. Guardians Of The Galaxy Vol. 2')
}
	favMovie4()

	let rottenTomato4 = function(){
	console.log('Rotten Tomatoes Rating: 85%')
}
	rottenTomato4()

	let favMovie5 = function(){
		console.log('5. DOCTOR STRANGE IN THE MULTIVERSE OF MADNESS')
}
	favMovie5()

	let rottenTomato5 = function(){
	console.log('Rotten Tomatoes Rating: 74%')
}
	rottenTomato5()


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


	function printFriends(){
		alert("Hi! Please add the names of your friends.");
		console.log('You are friends with: ')
	}
printFriends()

	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 


